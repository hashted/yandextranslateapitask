package com.ilin.bostongene.yandextranslateapitask;

import com.ilin.bostongene.yandextranslateapitask.yandextranslate.Translator;
import org.apache.commons.cli.*;

import java.io.PrintWriter;

public class CliApp {

    public static void main(String[] args) {

        Option optHelp = new Option("h", "help", false, "print this help message");
        optHelp.setArgs(0);

        Option optTranslate = new Option("t", "translate", true,
                "translate text EN-RU");
        optTranslate.setArgs(1);

        Options options = new Options();
        options
                .addOption(optHelp)
                .addOption(optTranslate);

        CommandLineParser cmdParser = new DefaultParser();
        CommandLine cmdLine = null;

        try {
            cmdLine = cmdParser.parse(options, args);
        } catch (UnrecognizedOptionException uoex) {
            System.err.println(uoex);
            System.out.println("Use java -jar YandexTranslateApiTask.jar -h for more details.");
            return;
        } catch (Exception ex) {
            System.err.println("Parsing failed. Reason: " + ex.getMessage());
            return;
        }

        if (cmdLine.hasOption("h")) {
            final String commandLineSyntax = "java -jar YandexTranslateApiTask.jar";
            final PrintWriter writer = new PrintWriter(System.out);
            final HelpFormatter helpFormatter = new HelpFormatter();
            helpFormatter.printHelp(writer, 80, commandLineSyntax, "where options include:", options, 4, 4, null, true);
            writer.flush();
        } else if (cmdLine.hasOption("t")) {
            String text = cmdLine.getOptionValue("t");
            System.out.println(Translator.doPost(text));
        }
    }
}