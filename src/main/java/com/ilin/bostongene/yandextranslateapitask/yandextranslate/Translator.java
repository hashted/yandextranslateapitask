package com.ilin.bostongene.yandextranslateapitask.yandextranslate;

import java.net.URI;

import org.springframework.http.HttpEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

public class Translator {

    private static URI yandexTranslateUri = URI.create("https://translate.yandex.net/api/v1.5/tr.json/translate");
    private static String API_KEY = "trnsl.1.1.20181223T101320Z.61f5f5d52e326171.6a5d133359b98a8d16d2cab6f9387cba2c74a676";
    private static MultiValueMap queryParams = new LinkedMultiValueMap<String, String>();
    private static MultiValueMap headers = new LinkedMultiValueMap<>();

    static {
        queryParams.add("key", API_KEY);
        queryParams.add("lang", "en-ru");
        queryParams.add("format", "plain");

        headers.add("Content-Type", "application/x-www-form-urlencoded");
    }

    public static String doPost(String text) {

        UriComponents uri = UriComponentsBuilder
                .fromUri(yandexTranslateUri)
                .queryParams(queryParams)
                .build();

        String uriString = uri.toUriString();

        StringBuilder bodyText = new StringBuilder("text=");
        bodyText.append(text);

        HttpEntity<String> request = new HttpEntity<>(bodyText.toString(), headers);

        try {

            Response response = new RestTemplate().postForObject(uriString, request, Response.class);

            if(response.getCode() == 200) {
                StringBuilder stringBuilder = new StringBuilder();
                stringBuilder.append("Переведено сервисом \"Яндекс.Переводчик\" http://translate.yandex.ru/:\n");
                stringBuilder.append(response.getText()[0]);
                return stringBuilder.toString();
            } else {
                return "Yandex.Translate response code: " + response.getCode();
            }
        } catch (HttpClientErrorException e) {
            return "Http status code: " + e.toString();
        }
    }
}
