Source code for BostonGene Junior task #3 solved by Nikolay Ilin.

Maven based project.

# Getting started
Compile the application sources:
```sh
mvn compile
```
Create a JAR file:
```sh
mvn package
```

# Using examples
```
java -jar YandexTranslateApiTask.jar -h
java -jar YandexTranslateApiTask.jar -t "hello"
```